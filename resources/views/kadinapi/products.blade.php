@php
$key='';
$kat=[];
$pro=[];
$produk =[];
$data = [];
$result=[];
/*
$product->ProductCategory->id
$product->ProductCategory->name
$product->id
$product->name
*/
$i=0;
$idx = 0;
foreach ($products as $product) {
    $ci = $product->ProductCategory->id;
    $cn = $product->ProductCategory->name;
    $pi = $product->id;
    $pn = $product->name;
// echo $product->ProductCategory->id . ' : ' . $product->ProductCategory->name . ' : ' . $product->id . ' : ' .$product->name . ' <hr> '; 
    
    if($key == $ci){
        array_push($produk,['id'=>$pi , 'name'=>$pn]);
        usort($produk, fn($a, $b) => $a['name'] <=> $b['name']);
        $pro[$idx]=$produk;
    }else{
        // kelola kategori
        $key = $ci;
        $idx = $i++;
        $kat[$idx] = ['id'=>$ci,'name'=>$cn];
        // Kelola produk
        $produk=[];
        array_push ($produk,['id'=>$pi , 'name'=>$pn]);
        $pro[$idx] = $produk;
    }

    
}

foreach ($kat as $i=>$pk) {
    array_push($data,[ 'kat'=>$pk,'pro'=>$pro[$i] ]);
}
echo json_encode($data,JSON_PRETTY_PRINT);

@endphp
