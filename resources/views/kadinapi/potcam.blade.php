{{-- Data Potensi Produk di Kecamatan X --}}
@php $i = 0; @endphp
@foreach ($products as $product)
    {{-- $product->product_id.":".$product->Product->name.":".$product->productCapacity --}}
    <tr>
        <td class='text-right'>{{ ++$i }}.</td>
        <td>{{ $product->Product->name }}</td>
        <td class='text-right'>{{ $product->productCapacity }}</td>
    </tr>
@endforeach