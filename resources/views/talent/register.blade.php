@extends('layouts.client')

@section('main')
<div class="row">
    <div class="col-lg-6 mx-auto">
        @include('talent.form')
    </div>
</div>
@endsection
@section('modals')

@endsection
@section('skripsi')
<script>
    $('#keahlian').tokenfield({
        autocomplete:{
            source:['las listrik','las karbit','mekanik mobil','mekanik motor','jahit','tata rambut','tukang kayu','tukang batu','operator komputer','teknisi komputer','programmer komputer'],
            delay:100
        },
        showAutocompleteOnFocus: true
    });
</script>
@endsection
