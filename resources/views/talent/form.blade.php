<h3 class="py-5 text-center">Formulir Head Hunter</h3>
<form action="{{ route('talents.store') }}" method="post" class="py-3 mb-5">
@csrf
<input type="hidden" id="redir" name="redir" value="{{ $request->path() }}">
<div class="form-group row">
    <label for="nik" class="col-sm-3">Nomor KTP</label>
    <div class="col-sm-9">
        <input type="text" name="nik" id="nik" class="form-control" value="">
    </div>
</div>
<div class="form-group row">
    <label for="namaLengkap" class="col-sm-3">Nama Lengkap</label>
    <div class="col-sm-9">
        <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" value="">
    </div>
</div>
<div class="form-group row">
    <label for="jnKelamin" class="col-sm-3">Jenis Kelamin</label>
    <div class="col-sm-9">
        <select name="jnKelamin" id="jnKelamin" class="form-control">
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="templatLahir" class="col-sm-3">Tempat Lahir</label>
    <div class="col-sm-9">
        <input type="text" name="templatLahir" id="templatLahir" class="form-control" value="Banjarnegara">
    </div>
</div>
<div class="form-group row">
    <label for="tanggalLahir" class="col-sm-3">Tanggal Lahir</label>
    <div class="col-sm-9">
        <input type="date" name="tanggalLahir" id="tanggalLahir" class="form-control" value="1990-01-01">
    </div>
</div>
<div class="form-group row">
    <label for="alamat" class="col-sm-3">Alamat</label>
    <div class="col-sm-9">
        <input type="text" name="alamat" id="alamat" class="form-control" value="">
    </div>
</div>
<div class="form-group row">
    <label for="pendidikanTerakhir" class="col-sm-3">Pendidikan Terakhir</label>
    <div class="col-sm-9">
        <select name="pendidikanTerakhir" id="pendidikanTerakhir" class="form-control">
            <option value="SLTP">SMP / MTs Sederajat</option>
            <option value="SLTA">SMA / SMK / MA Sederajat</option>
            <option value="Diploma 3">Diploma 3</option>
            <option value="Sarjana">Sarjana S1</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="keahlian" class="col-sm-3">Keahlian</label>
    <div class="col-sm-9">
        <input type="text" name="keahlian" id="keahlian" class="form-control" value="">
        <small>Pisahkan dengan tanda koma</small>
    </div>
</div>
{{-- nik , namaLengkap , jnKelamin , tempatLahir , tanggalLahir , alamat , pendidikanTerakhir ,  keahlian , email , telepon , deskripsi --}}
<div class="form-group row">
    <label for="email" class="col-sm-3">Email aktif</label>
    <div class="col-sm-9">
        <input type="emai" name="email" id="email" class="form-control" value="">
    </div>
</div>
<div class="form-group row">
    <label for="telepon" class="col-sm-3">Nomor Telepon</label>
    <div class="col-sm-9">
        <input type="text" name="telepon" id="telepon" class="form-control" value="">
    </div>
</div>
<div class="form-group row">
    <label for="deskripsi" class="col-sm-3">Deskripsi</label>
    <div class="col-sm-9">
        <p><small>Deskripsikan diri Anda</small></p>
        <textarea name="deskripsi" id="deskripsi" class="form-control" style="resize:none;" rows="6"></textarea>
    </div>
</div>
<div class="text-center">
    <input type="submit" value="Daftar!" class="btn btn-primary btn-sm">
</div>
</form>