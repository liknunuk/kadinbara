@extends('layouts.client')

@section('main')
<header class="py-5">
    <div class="container px-lg-5">
        <div class="p-4 p-lg-5 bg-light rounded-3 text-center">
            <div class="m-4 m-lg-5">
                <h1 class="display-5 fw-bold">Head Hunter</h1>
                <p class="fs-4">Halaman interaksi pengusaha dan pencari kerja di wilayah Kabupaten Banjarnegara</p>
                <a href="{{ url('/talent/register') }}" class="btn btn-sm btn-primary">Daftar!</a>
            </div>
        </div>
    </div>
</header>
<div class="row">
    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center" id="skills">

    </div>
</div>
@endsection
@section('modals')

@endsection
@section('skripsi')
<script>
    $('#keahlian').tokenfield({
        autocomplete:{
            source:['las listrik','las karbit','mekanik mobil','mekanik motor','jahit','tata rambut','tukang kayu','tukang batu','operator komputer','teknisi komputer','programmer komputer'],
            delay:100
        },
        showAutocompleteOnFocus: true
    });

    $(document).ready( function(){
        $.getJSON("{{ url('api/talentas') }}" , function(resp){
            $("#skills div").remove();
            $.each(resp.skills , function(i,data){
                $("#skills").append(`
                <div class="card mx-3 my-3">
                    <div class="card-body text-center">
                        <img src="https://static.vecteezy.com/system/resources/previews/016/325/062/original/head-hunter-male-icon-design-free-vector.jpg" style="width:120px;  ">
                    </div>
                    <div class="card-header text-center">
                        <strong>${data.skill}</strong>
                    </div>
                </div>
                `);
            })
        })
    })

</script>
@endsection
