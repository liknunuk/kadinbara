<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Kadin Bara</title>
        <!-- Favicon-->
        <link rel="icon shortcut" type="image/x-icon" href={{ asset("img/inakadin.png") }} />
        <!-- bootstrap 4 CDN -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <!-- jquery-ui -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />
        {{-- TokenField Styke --}}
        {{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  --}}
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
            <!-- Core theme CSS (includes Bootstrap)-->
        <link href="/css/client.css" rel="stylesheet" />
    </head>
    <body>
        {{-- Jumbotron --}}
        <div class="jumbotron jumbotron-fluid kadintron">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <img src="{{ asset('img/barakadin.png') }}" alt="bara kadin">
                    </div>
                    <div class="col-lg-10 py-3" id="kadin-brand">
                        <h1 class="display-4">KADINBARA</h1>
                        <p class="lead">Kamar Dagang dan Industri Kabupaten Banjarnegara</p>
                    </div>
                </div>
            </div>
          </div>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg">
            <div class="container px-lg-5">
                <a class="navbar-brand" href="{{ url('/client') }}">Kadin Banjarnegara</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#kadinClient" aria-controls="kadinClient" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="kadinClient">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{ url('/') }}">Beranda</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://kadinbara.or.id">Portal</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('/talent') }}">Pencari Kerja</a></li>
                    </ul>
                </div>
            </div>
        </nav>
            @yield('main')                
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Kadin Banjarnegara 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <!-- bootstrap script -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <!-- jquery-ui script -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        {{-- tokenfield script --}}
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>

        {{-- <script src="{{ asset('library/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('library/popper.js/dist/umd/popper.js') }}"></script>
        <script src="{{ asset('library/tooltip.js/dist/umd/tooltip.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script> --}}

        <!-- Core theme JS-->
        <script src="/js/client.js"></script>
        @yield('skripsi')
    </body>
</html>
