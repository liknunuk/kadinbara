<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('kadin') }}">KADIN3304</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('kadin') }}"><img src="{{ url('img/inakadin.png') }}" width="60px" alt=""></a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Panel Utama</li>
            <li class="nav-item dropdown {{ $type_menu === 'kadin' ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fa-solid fa-id-card"></i></i><span>Anggota</span></a>
                <ul class="dropdown-menu">
                    <li class='{{ Request::is('anggota-person') ? 'active' : '' }}'>
                        <a class="nav-link"
                            href="{{ url('anggota-person') }}">Personil</a>
                    </li>
                    <li class="{{ Request::is('anggota-badan-usaha') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ url('anggota-badan-usaha') }}">Badan Usaha</a>
                    </li>
                </ul>
            </li>
            <li class="menu-header">Bisnis</li>
            <li class="nav-item dropdown {{ $type_menu === 'bisnis' ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"
                    data-toggle="dropdown"><i class="fa-solid fa-briefcase"></i> <span>Komoditas</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('kategori-produk') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ url('kategori-produk') }}">Kategori Produk</a>
                    </li>
                    <li class="{{ Request::is('produk') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ url('produk') }}">Produk</a>
                    </li>
                    
                </ul>
            </li>
            {{-- blank page --}}
            {{--             
            <li class="{{ Request::is('blank-page') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ url('blank-page') }}"><i class="far fa-square"></i> <span>Blank Page</span></a>
            </li>
             --}}
            <li class="nav-item dropdown {{ $type_menu === 'bursa' ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fa-solid fa-arrows-rotate"></i></i> <span>Bursa</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('peta-komoditas') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ url('peta-komoditas') }}">Peta Komoditas</a>
                    </li>
                    <li class="{{ Request::is('bursa-komoditas') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ url('bursa-komoditas') }}">Bursa Komoditas</a>
                    </li>
                    
                </ul>
            </li>
            
            <li class="nav-item dropdown" {{ $type_menu === 'Talent' ? 'active' : '' }}>
                <a href="#"class="nav-link has-dropdown">
                    <i class="fa-solid fa-arrows-rotate"></i></i> <span>Pencari Kerja</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('Talent') ? 'active' : '' }}">
                        <a href="{{ url('talents') }}" class="nav-link">Pencari Kerja</a>
                    </li>
                </ul>
            </li>

            <li>
                <a class="nav-link"
                    href="{{ url('cek') }}"><i class="far fa-square"></i> <span>Stisla Templates</span></a>
            </li>
            {{-- <li class="nav-item dropdown">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fas fa-map-marker-alt"></i> <span>Google
                        Maps</span></a>
                <ul class="dropdown-menu">
                    <li><a href="gmaps-advanced-route.html">Advanced Route</a></li>
                    <li><a href="gmaps-draggable-marker.html">Draggable Marker</a></li>
                    <li><a href="gmaps-geocoding.html">Geocoding</a></li>
                    <li><a href="gmaps-geolocation.html">Geolocation</a></li>
                    <li><a href="gmaps-marker.html">Marker</a></li>
                    <li><a href="gmaps-multiple-marker.html">Multiple Marker</a></li>
                    <li><a href="gmaps-route.html">Route</a></li>
                    <li><a href="gmaps-simple.html">Simple</a></li>
                </ul>
            </li> --}}
            
        </ul>

        <div class="hide-sidebar-mini mt-4 mb-4 p-3">
            {{-- <a href="https://getstisla.com/docs"
                class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
            </a> --}}
        </div>
    </aside>
</div>
