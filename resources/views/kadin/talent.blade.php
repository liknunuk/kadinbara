@extends('layouts.app-kadin')

@section('title', 'General Dashboard')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
            </div>
            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>
                {{-- Baris Tabel --}}
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <a href="#" class="badge badge-primary mb-3 p-3">
                                        <i class="fa-solid fa-plus"></i> Talent
                                    </a>
                                    <table class="table table-bordered table-sm">
                                        <thead>
                                            <tr>
                                                <th>Nama Lengkap</th>
                                                <th>Tempat &amp; Tanggal lahir</th>
                                                <th>Alamat</th>
                                                <th>Keahlian</th>
                                                <th>Detil</th>
                                            </tr>
                                        </thead>
                                        <tbody id="talentsData">
                                            @foreach ($talents as $talent)
                                                <tr>
                                                    <td>{{ $talent->namaLengkap }}</td>
                                                    <td>{{ $talent->tempatLahir . ', ' . $talent->tanggalLahir  }}</td>
                                                    <td>{{ $talent->alamat }}</td>
                                                    <td>{{ $talent->keahlian }}</td>
                                                    <td>
                                                        <a href="{{ route("talents.show",$talent->id) }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-circle-info"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
@endpush
