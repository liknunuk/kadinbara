@extends('layouts.app-kadin')

@section('title', 'Form Kadin')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/selectric/public/selectric.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('library/jquery-ui-dist/jquery-ui.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header"><h1>{{ $title }} <span id="ProductId"></span></h1></div>
            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>
                {{-- Baris Formulir --}}
                <div class="row">
                    <div class="col-md-6 mx-auto">
                    @if ($map == null)
                        <form action="{{ route('peta-komoditas.store') }}" method="post" enctype="multipart/form-data">                        
                    @else
                        <form action="{{ route('peta-komoditas.update',$map->id) }}" method="post" enctype="multipart/form-data">                        
                        @method('PUT');
                    @endif
                            @csrf

                            {{-- elemen --}}
                            {{-- kecamatan,productCapacity,product_id,company_id; --}}
                            <input type="hidden" name="product_id" id="product_id">
                            <div class="form-group">
                                <label>Product ID</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            PID
                                        </div>
                                    </div>
                                    <input type="text" id="product_name" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kecamatan</label>
                                
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-building"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="kecamatan" id="kecamatan" placeholder="Kecamatan" list="loKecamatan">
                                    <datalist id="loKecamatan"></datalist>
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Kapasitas Produksi (Kg)</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-scale-balanced"></i>
                                        </div>
                                    </div>
                                    <input type="number" class="form-control" name="productCapacity" id="productCapacity" placeholder="Kapasitas Produksi">
                                </div>
                            </div>
                            {{-- elemen --}}
                            <input type="hidden" name="company_id" id="company_id">
                            <div class="form-group">
                                <label>Perusahaan Produsen</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-regular fa-building"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="seekCompany" placeholder="Perusahaan Produsen">
                                </div>
                            </div>
                            {{-- elemen --}}
                            {{-- kecamatan , productCapacity , product_id , company_id, --}}
                            
                            <div class="form-group d-flex justify-content-end px-3">
                                <input type="reset" value="Ulangi" class="btn btn-warning mx-2">
                                <input type="submit" value="Simpan" class="btn btn-success mx-2">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('library/cleave.js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.js') }}"></script>

    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('js/page/forms-advanced-forms.js') }}"></script> --}}
    <script>

        $(document).ready( function(){
            let PID = localStorage.getItem('ProductId');
            let datas = PID.split('_');
            let produ = datas[1].split('-');
            $.getJSON("{{ url('/api/prodinfo') }}/"+produ[1],
                function(resp){
                    // console.log(resp);
                $("#product_name").val(resp.produk);
                $('#ProductId').text('Kategory ' + resp.kategori + ' Produk ' + resp.produk );
                }
            )
            // output
            $("#product_id").val(produ[1]);
            // localStorage.setItem('ProductId','');
        })
        var kecamatan=[
            'Banjarmangu',
            'Banjarnegara',
            'Batur',
            'Bawang',
            'Kalibening',
            'Karangkobar',
            'Madukara',
            'Mandiraja',
            'Pagedongan',
            'Pagentan',
            'Pandanarum',
            'Pejawaran',
            'Punggelan',
            'Purwanegara',
            'Purwareja-Klampok',
            'Rakit',
            'Sigaluh',
            'Susukan',
            'Wanadadi',
            'Wanayasa'
        ];

        $('#kecamatan').autocomplete({ source:kecamatan} );
        
        $('#seekCompany').autocomplete({
            minLength:2,
            source: function(request,response){
                $.getJSON(
                    '{{ url('/api/company/') }}/' + request.term,
                    function(data){
                        // console.log(data);
                        $.each(data , function (i,opts){

                            response($.map(opts,function(opt){
                                console.log(opt);
                                return{
                                    label: opt.name,
                                    value: opt.id
                                }
                            }))

                        })
                    })
            },
            focus: function(event,ui){
                $(this).val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {                
            $('#company_id').val(ui.item.value);
            return false;
            }
        });
        
    </script>
@endpush