@extends('layouts.app-kadin')

@section('title', 'Form Kadin')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/selectric/public/selectric.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header"><h1>{{ $title }}</h1></div>
            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>
                {{-- Baris Formulir --}}
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        @if ($company == null)
                            <form action="{{ route('anggota-badan-usaha.store') }}" method="post" enctype="multipart/form-data">                            
                        @else
                            <form action="{{ route('anggota-badan-usaha.update',$company->id) }}" method="post" enctype="multipart/form-data">
                                @method('PUT')                            
                        @endif
                            @csrf
                            {{-- elemen --}}
                            <input type="hidden" name="id" id="id" value="{{ $company==null ? "" : $company['id'] }}">
                            <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-building"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nama Perusahaan"
                                    value="{{ $company == null ? "" : $company['name'] }}">
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Alamat Rumah</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-location-dot"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Alamat Rumah"
                                    value="{{ $company == null ? "" : $company['address'] }}">
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Alamat Email</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-at"></i>
                                        </div>
                                    </div>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Alamat Email"
                                    value="{{ $company == null ? "" : $company['email'] }}">
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Nomor Telepon</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-phone"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control phone" name="phone" id="phone" placeholder="Nomor Telepon"
                                    value="{{ $company == null ? "" : $company['phone'] }}">
                                </div>
                            </div>
                            {{-- name,address,bussinesCategory,phone,email,member_id --}}
                            {{-- elemen optional --}}
                            <input type="hidden" name="member_id" id="member_id" value="{{ $company == null ? "" : $company['member_id'] }}">
                            <div class="form-group">
                                <label>Nama Pemilik Perusahaan</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-user"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control seek_owner" id="seek_owner" placeholder="Nama Pemilik Perusahaan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Bidang Usaha</label>
                                <div class="selectgroup selectgroup-pills" id="bidangUsaha">

                                    {{-- @php
                                        $buscat = range(1,10);
                                    @endphp
                                    @foreach ($buscat as $i=>$bc)

                                    <label class="selectgroup-item">
                                        <input type="checkbox" value="Bidang{{ $bc }}" class="selectgroup-input buscat" >
                                        <span class="selectgroup-button">Bidang {{ sprintf('%02d',$bc) }}</span>
                                    </label>
                                        
                                    @endforeach --}}
                                    
                                </div>
                                <input type="hidden" name="businessCategory" id="buscats" value="{{ $company == null ? "" : $company['businnesCategory'] }}">
                                <div id="bidangs"></div>
                            </div>
                            <div class="form-group d-flex justify-content-end px-3">
                                <input type="reset" value="Ulangi" class="btn btn-warning mx-2">
                                <input type="submit" value="Simpan" class="btn btn-success mx-2">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('library/cleave.js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.js') }}"></script>

    <script>

        $(document).ready(function(){
            $.getJSON('{{ url('/api/procats') }}' , function(resp){
                $("#bidangUsaha label").remove();
                $.each(resp.kategori , function(i,data){
                    $('#bidangUsaha').append(`
                    <label class="selectgroup-item">
                        <input type="checkbox" value="${data.name}" class="selectgroup-input buscat" >
                        <span class="selectgroup-button">Bidang ${data.name}</span>
                    </label>
                    `);
                })
            })
        })
        // Cleave.Js Instantce
        var cleavePC = new Cleave(".phone", {
            delimiter: "-",
            blocks: [4, 4, 4],
            uppercase: true,
        });

        $('.seek_owner').autocomplete({
            minLength:2,
            source: function(request,response){
                $.getJSON(
                    '{{ url('/api/owner/') }}' + '/' + request.term,
                    function(data){
                        // console.log(data);
                        $.each(data , function (i,opts){

                            response($.map(opts,function(opt){
                                console.log(opt);
                                return{
                                    label: opt.id +'-'+ opt.fullName,
                                    value: opt.id
                                }
                            }))

                        })
                    })
            },
            focus: function(event,ui){
                $(this).val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {                
            $('#member_id').val(ui.item.value);
            return false;
            }
        });

        $("#bidangUsaha").on('change','.buscat', (e) => {
            let buscats = $('.buscat:checked');
            let buscat, bc='';
            // let val = e.currentTarget.value;
            // alert(val);
            for ( buscat of buscats ){
                bc = bc + ',' + buscat.value ;
            }
            console.log(bc);
            $('#buscats').val(bc);
        })
    </script>
@endpush