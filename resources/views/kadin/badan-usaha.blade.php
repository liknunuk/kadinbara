@extends('layouts.app-kadin')

@section('title', 'Kadin Banjarnegara')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
            </div>

            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>

                {{-- Baris Tabel --}}
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <a href="{{ url('anggota-badan-usaha/create') }}" class="badge badge-primary mb-3 p-3">
                                        <i class="fa-solid fa-plus"></i> Badan Usaha
                                    </a>
                                    <table class="table table-bordered table-sm">
                                        <thead class='bg-success text-dark text-center'>
                                            <tr>
                                                <th>Nama Perusahaan</th>
                                                <th>Alamat Domisili</th>
                                                <th>Jenis Usaha</th>
                                                <th>Nomor Telephone</th>
                                                <th>Alamat Surat Elektronik</th>
                                                <th>Kendali</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($companies as $company)
                                                <tr>
                                                    <td>{{ $company->name }}</td>
                                                    <td>{{ $company->address }}</td>
                                                    <td>{{ $company->businessCategory }}</td>
                                                    <td>{{ $company->phone }}</td>
                                                    <td>{{ $company->email }}</td>
                                                    <td width="150">
                                                        <form action="{{ route('anggota-badan-usaha.destroy',$company->id) }}" method="POST">
   
                                                            <a class="btn btn-sm mx-1 btn-primary" href="{{ route('anggota-badan-usaha.show',$company->id) }}"><i class="fa-solid fa-building"></i></a>
                                            
                                                            <a class="btn btn-sm mx-1 btn-warning" href="{{ route('anggota-badan-usaha.edit',$company->id) }}"><i class="fa-solid fa-pen-nib"></i></a>
                                           
                                                            @csrf
                                                            @method('DELETE')
                                              
                                                            <button type="submit" class="btn btn-sm mx-1 btn-danger"><i class="fa-solid fa-hammer"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <p>Pagination Block</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
@endpush
