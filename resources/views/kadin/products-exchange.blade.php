@extends('layouts.app-kadin')

@section('title', 'General Dashboard')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Bursa Komoditas</h1>
            </div>

            <div class="section-body" id="bursa">                
                                   
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('js/page/index-0.js') }}"></script> --}}
    <script>
        $(document).ready( function(){
            $("#bursa").html('');
            $.getJSON(
                "{{ url('/api/latestExc') }}",
                function(resp){
                    $.each(resp , function(i,data){

                        $('#bursa').append(`
                        <div class="page-label">${data.produk.id} - ${data.produk.name}</div> 
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr class='bg-secondary text-dark text-center'>
                                    <th>Tanggal Permintaan</th>
                                    <th>Nama Pemesan</th>
                                    <th>Kota Pemesan</th>
                                    <th>Volume Pesanan</th>
                                    <th>Batas Pemenuhan</th>
                                </tr>
                            </thead>
                            <tbody id="bursa_${data.produk.id}"></tbody>
                        </table>
                        `);
                        $.each(data.orders, function(i,order){
                            $('#bursa_'+data.produk.id).append(`
                            <tr>
                                <td>${order.orderDate}</td>
                                <td>${order.name}</td>
                                <td>${order.city}</td>
                                <td class='text-right'>${order.quota}</td>
                                <td class='text-right'>${order.dueDate}</td>
                            </tr>
                            `);
                        });
                    })
                }
            )
        })
    </script>

@endpush
