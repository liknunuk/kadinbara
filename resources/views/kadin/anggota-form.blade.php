@php
    $ms = array();
    if($member == null){
        $ms=[
            'ig'=>'',
            'fb'=>'',
            'tw'=>'',
            'yt'=>'',
            'tt'=>''
    ];
        // $ms['ig']='';
        // $ms['fb']='';
        // $ms['tw']='';
        // $ms['yt']='';
        // $ms['tt']='';
    }else{
        $medsoc = json_decode($member->medsocs);
    }
@endphp
@extends('layouts.app-kadin')

@section('title', 'Form Kadin')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/selectric/public/selectric.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header"><h1>{{ $title }}</h1></div>
            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>
                {{-- Baris Formulir --}}
                <div class="row">
                    
                    <div class="col-md-6 mx-auto">
                        {{-- @dd($member); --}}
                        @if ($member == null )
                            <form action="{{ route('anggota-person.store') }}" method="post" enctype="multipart/form-data">
                        @else
                            <form action="{{ route('anggota-person.update',$member->id) }}" method="post" enctype="multipart/form-data">
                            @method('PUT')
                        @endif

                            @csrf
                            {{-- elemen --}}
                            <input type="hidden" name="id" value="{{ $member == null ? "" : $member->id }}">
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-regular fa-circle-user"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="fullName" id="fullName" placeholder="Nama Lengkap"
                                    value = "{{ $member == null ? "" : $member['fullName'] }}"
                                    >
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Alamat Rumah</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-location-dot"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Alamat Rumah"
                                    value = "{{ $member == null ? "" : $member['address'] }}"
                                    >
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Alamat Email</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-at"></i>
                                        </div>
                                    </div>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Alamat Email"
                                    value = "{{ $member == null ? "" : $member['email'] }}"
                                    >
                                </div>
                            </div>
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Nomor Telepon</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-solid fa-mobile-screen"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control purchase-code" name="phone" id="phone" placeholder="0812-3456-7890" 
                                    value = "{{ $member == null ? "" : $member['phone'] }}"
                                    >
                                </div>
                            </div>
                            
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Media Social</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-brands fa-instagram"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control socmed" id="Instagram" placeholder="Instagram"
                                    value="{{ $member == null ? "" : $medsoc->Instagram }}">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-brands fa-facebook"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control socmed" id="Facebook" placeholder="Facebook"
                                    value="{{ $member == null ? "" : $medsoc->Facebook }}">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-brands fa-twitter"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control socmed" id="Twitter" placeholder="Twitter"
                                    value="{{ $member == null ? "" : $medsoc->Twitter }}">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-brands fa-youtube"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control socmed" id="Youtube" placeholder="Youtube"
                                    value="{{ $member == null ? "" : $medsoc->Youtube }}">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-brands fa-tiktok"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control socmed" id="Tiktok" placeholder="Tiktok"
                                    value="{{ $member == null ? "" : $medsoc->Tiktok }}">
                                </div>
                            </div>
                            <input type="hidden" name="medsocs" id="socmed"
                            value="{{ $member == null ? "" : $member->medsocs }}"
                            >
                            <div class="form-group d-flex justify-content-end px-3">
                                <input type="reset" value="Ulangi" class="btn btn-warning mx-2">
                                <input type="submit" value="Simpan" class="btn btn-success mx-2">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('library/cleave.js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>

    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('js/page/forms-advanced-forms.js') }}"></script> --}}
    <script>
        // Cleave.Js Instantce
        var cleavePC = new Cleave(".purchase-code", {
            delimiter: "-",
            blocks: [4, 4, 4],
            uppercase: true,
        });
        // Cleave.Js Instantce
        const socmed = new Object();
        $(".socmed").change( function(){
            let media = this.id;
            let addrs = this.value;
            socmed[media] = addrs;
            $('#socmed').val( JSON.stringify(socmed) );
        })
    </script>
@endpush