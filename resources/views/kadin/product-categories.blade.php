@extends('layouts.app-kadin')

@section('title', 'General Dashboard')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Kategori Produk</h1>
            </div>

            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>

                {{-- Baris Tabel --}}
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <form id="categoryForm" action="{{ route('kategori-produk.store') }}" method="post" class="form-inline">
                            @csrf
                            <input type="hidden" id="id" name="id" value="null">
                            {{-- elemen --}}
                            <label class="sr-only" for="name">Nama Kategori</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa-solid fa-registered"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nama Kategori">
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Tambah</button>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                {{-- @php
                                    $categories = range(1,20);
                                @endphp --}}
                                {{-- @dd($productCategories) --}}
                                @foreach ($productCategories as $i=>$category)                                
                                <!-- Button trigger modal -->
                                <button type="button" class="badge badge-pill badge-secondary mr-1 mt-2 p-2 text-dark categoryControl"  id="kat_{{ $category->id }}" style="min-width:150px;">
                                    {{ $category->name }}
                                </button>
                                {{--                                 
                                <span class="badge badge-pill badge-secondary mr-3 mt-2 p-2 text-dark">
                                    Kategori - {{ sprintf('%02d',$category) }}
                                </span>
                                 --}}
                                @endforeach
                            </div>
                            <div class="card-footer text-right">
                                <p>Pagination Block</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
    <script>
        let catId = "",
            catName ="";

        let eurl = "{{ url('kategori-produk') }}";
        $('.categoryControl').click( function(){
            $('#categoryControl').modal('show');
            let pk = this.id.split('_');
            catName = $(this).text().trim();
            catId=pk[1];
            let durl = "{{ route('kategori-produk.destroy',":catId") }}";
            durl = durl.replace(":catId",catId);
            // alert(durl);
            $('#eid').val(catId);
            $('#ename').val(catName);
            $('#eform').attr('action',eurl+'/'+catId);
            $('#dform').attr('action',durl);
            $('#product_category_id').val(catId);
            $('#product_name').val(catName);
        })
        $('#catChange').click(function(){
            $('input[name="id"]').val(catId);
            $('input[name="name"]').val(catName);
            $('#categoryControl').modal('toggle');
        })
    </script>
@endpush
{{-- Modals --}}

  
  <!-- Modal -->
  <div class="modal fade" id="categoryControl" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Kontrol Kategori Produk</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="text-center">
                <section>
                    <h4 class="text-center">Tambah Produk</h4>
                    <form action="{{ route('produk.store') }}" method="post" class="inline-form">
                        @csrf
                        <input type="hidden" name="product_category_id" id="product_category_id">
                        <div class="form-group mb-0">
                            <input type="text" id="product_name" class="form-control" readonly>
                        </div>
                        <div class="form-group mb-0">                            
                            <input type="text" name="name" id="name" class="form-control" placeholder="Nama Produk">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa-solid fa-plus"></i> Produk 
                            </button>
                        </div>
                    </form>
                </section>
                <section>
                    <h4 class="text-center">Edit Kategori</h4>
                    <form id="eform" method="post" class="inline-form">
                        @csrf
                        {{ method_field('PUT') }}
                        
                        <div class="form-group">
                            <input type="hidden" name="id" id="eid" class="form-control" readonly>
                        </div>
                        <div class="form-group mb-0">                            
                            <input type="text" name="name" id="ename" class="form-control" placeholder="Nama Produk">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-warning">
                                <i class="fa-solid fa-edit"></i> Kategori 
                            </button>
                        </div>
                    </form>
                </section>
                <section>
                    <form id="dform" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">
                            <i class="fa-solid fa-trash"></i> Kategori
                        </button>
                    </form>
                    
                </section>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary text-dark" data-dismiss="modal">Tutup</button>
          {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
        </div>
      </div>
    </div>
  </div>