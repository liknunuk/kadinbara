@extends('layouts.app-kadin')

@section('title', $title)

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>
            </div>
            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>
                {{-- Baris Tabel --}}
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <a href="{{ route('talents.index') }}" class="badge badge-primary mb-3 p-3">
                                        <i class="fa-solid fa-left-long"></i> Kembali
                                    </a>
                                    <table class="table table-bordered table-sm">
                                        {{-- id,nik,namaLengkap,jnKelamin,tempatLahir,tanggalLahir,alamat,pendidikanTerakhir,keahlian,email,telepon,deskripsi --}}
                                        <tbody id="talentData">
                                            <tr>
                                                <th>Nama Lengkap</th>
                                                <td>{{ $talent->namaLengkap }} - {{ $talent->jnKelamin }}</td>
                                            </tr>
                                            <tr>
                                                <th>Tempat, tanggal Lahir</th>
                                                <td>{{ $talent->tempatLahir }}, {{ $talent->tanggalLahir }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>{{ $talent->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <th>Pendidikan Terakhir</th>
                                                <td>{{ $talent->pendidikanTerakhir }}</td>
                                            </tr>
                                            <tr>
                                                <th>Kontak</th>
                                                <td>{{ $talent->email }}, {{ $talent->telepon }}</td>
                                            </tr>
                                            <tr>
                                                <th>Keahilian</th>
                                                <td>{{ $talent->keahlian }}</td>
                                            </tr>
                                            <tr>
                                                <th>Gambaran umum talent</th>
                                                <td>{{ $talent->deskripsi }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
@endpush
