@extends('layouts.app-kadin')

@section('title', 'Form Kadin')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/selectric/public/selectric.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header"><h1>{{ $title }}</h1></div>
            <div class="section-body">
                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>
                {{-- Baris Formulir --}}
                <div class="row">
                    
                    <div class="col-md-6 mx-auto">
                        {{-- @dd($produk); --}}
                        @if ($produk == null )
                            <form action="{{ route('produk.store') }}" method="post" enctype="multipart/form-data">
                        @else
                            <form action="{{ route('produk.update',$produk->id) }}" method="post" enctype="multipart/form-data">
                            @method('PUT')
                        @endif

                            @csrf
                            {{-- elemen --}}
                            <input type="hidden" name="id" value="{{ $produk == null ? "" : $produk->id }}">
                            {{-- elemen --}}
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa-regular fa-circle-user"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nama Lengkap"
                                    value = "{{ $produk == null ? "" : $produk->name }}" >
                                </div>
                            </div>
                            
                            <div class="form-group d-flex justify-content-end px-3">
                                <input type="reset" value="Ulangi" class="btn btn-warning mx-2">
                                <input type="submit" value="Simpan" class="btn btn-success mx-2">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('library/cleave.js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>

    <!-- Page Specific JS File -->
    {{-- <script src="{{ asset('js/page/forms-advanced-forms.js') }}"></script> --}}
    
@endpush