@extends('layouts.app-kadin')

@section('title', 'General Dashboard')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Produk</h1>
            </div>

            <div class="section-body">

                {{-- Sub Judul --}}
                <h2 class="section-title">Kamar Dagang dan Industri</h2>
                <p class="section-lead">Kabupaten Banjarnegara</p>

                {{-- Baris Tabel --}}
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="row" id="productData">
                                    
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <p>Pagination Block</p>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>
        </section>
    </div>
@endsection
@section('modals')
{{-- Modal --}}
<div class="modal fade" id="productControl" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Produk</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <section class="text-center mt-3">
            <form method="post" id="modalProduct">
                <a class="mx-2 btn btn-warning" id="chgProduct">
                    <i class="fa-solid fa-edit"></i> Ubah
                </a>
                @csrf
                @method('DELETE')
                <button type="submit" class="mx-2 btn btn-danger">
                    <i class="fa-solid fa-trash"></i> Hapus
                </button>
            </form>
          </section>
          <section class="text-center mt-3">
            <form action="{{ url('setFotoProduk') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="imageID">
            <div class="form-group">
                <input type="file" name="imgUrl" id="imgUrl">
            </div>
            <div class="form-group text-center my-1">
                <input type="submit" value="Unggah Foto" class="btn btn-danger">
            </div>
            </form>
          </section>
          <section class="text-center mt-3">
            <a href="{{ url('peta-komoditas/create') }}" class="btn btn-primary" id="addProductMap">
                <i class="fa-solid fa-plus"></i> Data Sebaran Produk
            </a>
            {{-- <a href="{{ url('peta-komoditas/show') }}" class="btn btn-info" id="getProductMap">
                <i class="fa-solid fa-binoculars"></i> Peta Sebaran Produk
            </a> --}}

          </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
    <script>
        let ProductId = "";
        // let url = {{ url('peta-komoditas/create') }};
        $(document).ready( function(){
            $.getJSON("{{ url('/api/kateprod') }}" , function(resp){
                $('#productData div.card').remove();
                $.each(resp , function(i,data){
                    // console.log(data);
                    $('#productData').append(`
                    <div class="col-sm-3 card">
                        <div class="card-header py-0">${data.kat.name}</div>
                            <ul class="list-group list-group-flush productList" id = "block_${data.kat.id}">`);
                    $.each(data.pro , function(i,product){
                        // console.log('kategori:',data.kat.name);
                        // console.log(product.name);
                        $("#block_"+data.kat.id).append(`
                            <li class="list-group-item" style="padding:0px 15px;">
                                <a href="javascript:void(0)" class="mb-2 badge badge-secondary text-dark productControl" id="Kategori-${data.kat.id}_Produk-${product.id}">
                                    ${product.name}
                                </a>
                            </li>
                        `);
                    })
                    // console.log(data.pro);
                    $('#productData').append(`
                            </ul>
                        </div>
                    </div>`);

                    // $.each(data.pro , function(i,prod){
                    //     })
                })

            })
        })
        // let durl = "{{ route('kategori-produk.destroy',":catId") }}";
        $('#productData').on('click','.productControl',function(){
            $('#productControl').modal('show');
            ProductId=this.id;
            localStorage.setItem('ProductId',ProductId);
            let kapro = ProductId.split('_');
            let prods = kapro[1].split('-');
            let rmurl = "{{ route('produk.destroy',":proid") }}";
            let churl = "{{ route('produk.edit',":pid") }}";

            rmurl=rmurl.replace(":proid",prods[1]);
            churl=churl.replace(":pid",prods[1]);
            $('#modalProduct').attr('action',rmurl);
            $('#chgProduct').attr('href',churl);
            $("#imageID").val(prods[1]);
        })

        
    </script>
    {{-- #Kategori-1_Produk-12 --}}
@endpush

  

