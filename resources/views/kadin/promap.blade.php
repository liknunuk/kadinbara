@extends('layouts.app-kadin')

@section('title', 'General Dashboard')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>PETA SEBARAN POTENSI PRODUK</h1>
            </div>
            <div class="section-body row" id="promap">
                {{-- < ?php $data=range(1,12); ?>
                @foreach ($data as $item)
                    <div class="card list-group-flush col-sm-3">
                        <li class="list-group-item bg-dark text-secondary">Group {{ $item }}</li>                        
                        @for ($i = 1; $i < 5; $i++)
                        <li class="list-group-item py-1">Item - {{ $i }}</li>                                
                        @endfor
                    </div>
                @endforeach --}}
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
    <script>
        $(document).ready(function(){
            $.getJSON("{{ url('api/promap') }}",function(resp){
                $('#promap div').remove();
                $.each(resp , function(i,data){
                    $('#promap').append(`
                    <div class="card list-group-flush col-sm-3" id="pm_${i}">
                        <li class="list-group-item bg-info text-dark py-1">${data.kecamatan}</li>
                    `);
                    $.each(data.potensi, function(j,produk){
                        $(`#pm_${i}`).append(`
                        <li class="list-group-item py-1">
                            ${produk.name}
                            <span class='float-right'>${produk.capacity}</span>
                        </li>
                        `);
                    });
                    $('#promap').append(`</div>`);
                })
            })
        })
    </script>
@endpush