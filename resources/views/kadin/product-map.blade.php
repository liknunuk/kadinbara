@extends('layouts.app-kadin')

@section('title', 'General Dashboard')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet"
        href="{{ asset('library/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Peta Komoditas</h1>
            </div>

            <div class="section-body border-1">
                <section>
                    <div class="row py-3">
                        <div class="col-md-6">
                            <h5>Pilih Kecamatan</h5>
                            <div class="form-group">
                                <input type="text" name="kecamatan" id="kecamatan" class="form-control bg-secondary">
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger" id="getPotCam">Lihat Potensi Produk</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Pilih Produk</h5>
                            <div class="form-group">
                                <input type="hidden" name="product_id" id="product_id">
                                <input type="text" name="produk" id="produk" class="form-control bg-secondary">
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger" id="getPotDuk">Lihat Potensi Per Kecamatan</button>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="mt-3">
                    <table class="table table-sm table-striped col-sm-6 mx-auto" id="tabPotCam" style="display:none;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Produk</th>
                                <th>Kapasitas</th>
                            </tr>
                        </thead>
                        <tbody id="dapotPDC"></tbody>
                    </table>
                    <table class="table table-sm table-striped col-sm-6 mx-auto" id="tabPotDuk" style="display:none;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kecamatan</th>
                                <th>Kapasitas</th>
                            </tr>
                        </thead>
                        <tbody id="dapotPMC"></tbody>
                    </table>
                </section>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/simpleweather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('library/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('library/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/index-0.js') }}"></script>
    <script>
        var kecamatan=[
            'Banjarmangu',
            'Banjarnegara',
            'Batur',
            'Bawang',
            'Kalibening',
            'Karangkobar',
            'Madukara',
            'Mandiraja',
            'Pagedongan',
            'Pagentan',
            'Pandanarum',
            'Pejawaran',
            'Punggelan',
            'Purwanegara',
            'Purwareja-Klampok',
            'Rakit',
            'Sigaluh',
            'Susukan',
            'Wanadadi',
            'Wanayasa'
        ];

        $('#kecamatan').autocomplete({ source:kecamatan} );
        $('#produk').autocomplete({
            minLength:2,
            source: function(request,response){
                $.getJSON(
                    '{{ url('/api/productSearch/') }}/' + request.term,
                    function(data){
                        // console.log(data);
                        $.each(data , function (i,opts){

                            response($.map(opts,function(opt){
                                // console.log(opt);
                                return{
                                    label: opt.name,
                                    value: opt.id
                                }
                            }))

                        })
                    })
            },
            focus: function(event,ui){
                $(this).val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {                
            $('#product_id').val(ui.item.value);
            return false;
            }
        })
        
        
        $('#kecamatan').focus( function(){
                $("#tabPotCam").show();
                $("#tabPotDuk").hide();
                $('#produk').val('');
            })

        $('#produk').focus(function(){
            $("#tabPotCam").hide();
            $("#tabPotDuk").show();
            $('#kecamatan').val('');
        })

        $('#getPotCam').click( function(){
            if($("#kecamatan").val() == ""){
                prefentDefault();
            }else{
                $.ajax({
                    url: "{{ url('/api/pdc') }}/"+$('#kecamatan').val(),
                    success: function(resp){
                        $('#dapotPDC tr').remove();
                        $('#dapotPDC').append(resp);
                    }
                })
            }
        })

        $('#getPotDuk').click( function(){
            // alert($("#product_id").val());
            if($("#produk").val() == ""){
                prefentDefault();
            }else{
                let no=1;
                $.ajax({
                    url: "{{ url('/api/pmc') }}/"+$('#product_id').val(),
                    success: function(resp){
                        $('#dapotPMC tr').remove();
                        $.each(resp.data , function(i,pot){
                            $('#dapotPMC').append(`
                            <tr>
                                <td class='text-right'>${no++}.</td>
                                <td>${pot.kecamatan}</td>
                                <td class='text-right'>${pot.productCapacity}</td>
                            </tr>
                            `)
                        })
                    }
                })
            }
        })
    </script>

@endpush
