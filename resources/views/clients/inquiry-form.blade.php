@extends('layouts.client')

@section('main')
<div class="row">
    <div class="col-6 mx-auto py-5">

        <h3>Order Form</h3>
        <form action="{{ route('bursa-komoditas.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            {{-- ORDER ID --}}
            <div class="form-group kadin-row">
                <label for="id" class="col-sm-4">ORDER ID</label>
                <div class="col-sm-8">
                    <input type="text" name="id" id="id" class="form-control" readonly>
                </div>
            </div>
    
            {{-- Tanggal Order --}}
            <div class="form-group kadin-row">
                <label for="inquiryDate" class="col-sm-4">Tanggal Order</label>
                <div class="col-sm-8">
                    <input type="date" name="inquiryDate" id="inquiryDate" class="form-control" readonly value="{{ date('Y-m-d') }}">
                </div>
            </div>
           
            {{-- Nama Pemesan --}}            
            <div class="form-group kadin-row">
                <label for="applicantName" class="col-sm-4">Nama Pemesan</label>
                <div class="col-sm-8">
                    <input type="text" name="applicantName" id="applicantName" class="form-control">
                </div>
            </div>
            
            {{-- alamat pemesan --}}
            <div class="form-group kadin-row">
                <label for="applicantAddress" class="col-sm-4">Alamat Pemesan</label>
                <div class="col-sm-8">
                    <input type="text" name="applicantAddress" id="applicantAddress" class="form-control">
                </div>
            </div>

            {{-- email pemesan --}}
            <div class="form-group kadin-row">
                <label for="applicantEmail" class="col-sm-4">Alamat Email</label>
                <div class="col-sm-8">
                    <input type="email" name="applicantEmail" id="applicantEmail" class="form-control">
                </div>
            </div>
    
            {{-- Nomor Telepon Pemesan --}}
            <div class="form-group kadin-row">
                <label for="applicantPhone" class="col-sm-4">Nomor Telepon / Mobile</label>
                <div class="col-sm-8">
                    <input type="text" name="applicantPhone" id="applicantPhone" class="form-control">
                </div>
            </div>
            
            {{-- Kota Pemesan --}}
            <div class="form-group kadin-row">
                <label for="applicantCity" class="col-sm-4">Kota</label>
                <div class="col-sm-8">
                    <input type="text" name="applicantCity" id="applicantCity" class="form-control">
                </div>
            </div>
    
            {{-- Propinsi Pemesan  --}}
            <div class="form-group kadin-row">
                <label for="applicantProvince" class="col-sm-4">Propinsi</label>
                <div class="col-sm-8">
                    <input type="text" name="applicantProvince" id="applicantProvince" class="form-control">
                </div>
            </div>
    
            {{-- ID Produk --}}
            <div class="form-group kadin-row">
                <label for="product_name" class="col-sm-4">Nama Produk</label>
                <div class="col-sm-8">
                    <input type="hidden" name="product_id">
                    <input type="text" name="product_name" id="product_name" class="form-control">
                </div>
            </div>
            {{-- Jumlah Pesanan --}}
            <div class="form-group kadin-row">
                <label for="orderQuota" class="col-sm-4">Jumlah Dipesan</label>
                <div class="col-sm-8">
                    <input type="number" name="orderQuota" id="orderQuota" class="form-control">
                </div>
            </div>
            
            {{-- Batas Pemenuhan --}}
            <div class="form-group kadin-row">
                <label for="dueDate" class="col-sm-4">Batas Pemenuhan</label>
                <div class="col-sm-8">
                    <input type="date" name="dueDate" id="dueDate" class="form-control">
                </div>
            </div>
            <input type="hidden" name="route" value="client">
            <div class="form-group d-flex justify-content-end">
                <button type="submit" class="btn btn-primary">Kirim Pesanan</button>
            </div>
        </form>
            {{--  --}}
            {{-- <div class="form-group row">
                <label for="xxx" class="col-sm-4"></label>
                <div class="col-sm-8">
                    <input type="text" name="xxx" id="xxx" class="form-control">
                </div>
            </div> --}}
    </div>
</div>
@endsection

@section('skripsi')
<script>
    $(document).ready(function(){
        const product_id = localStorage.getItem('product_id');
        const product_name = localStorage.getItem('product_name');
        $("input[name='product_id']").val(product_id);
        $("#product_name").val(product_name);
    })
</script>
@endsection
