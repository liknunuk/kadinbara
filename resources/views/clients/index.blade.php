@extends('layouts.client')

@section('main')
{{-- Jumbotron --}}
<!-- Header-->
<header class="py-5">
    <div class="container px-lg-5">
        <div class="p-4 p-lg-5 bg-light rounded-3 text-center">
            <div class="m-4 m-lg-5">
                <h1 class="display-5 fw-bold">A warm welcome!</h1>
                <p class="fs-4">Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
            </div>
        </div>
    </div>
</header>
<!-- Page Content-->
<section class="pt-4">
    <div class="container px-lg-5">
{{-- Jumbotron --}}
        @if (session('success'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
          </div>
        @endif
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center" id="apm">

        </div>
    </div> {{-- end of container --}}
</section>
@endsection

@section('skripsi')
<script>
    $(document).ready( function(){
       $('#apm div').remove();
       $.getJSON("{{ url('api/allProductMap') }}",function(resp){
        $.each(resp.data , function(i , data){
            // skrip begin
            if(data.imgUrl== null){
                imgSrc = "https://dummyimage.com/450x300/dee2e6/6c757d.jpg";
            }else{
                imgSrc = "{{ asset('/storage') }}/"+data.imgUrl;
            }
            $('#apm').append(`
                <div class="card h-100 border-0">
                    <!-- Product image-->
                    <img class="card-img-top" src="${imgSrc}" style="margin-top:15px;">
                    <!-- Product details-->
                    <div class="card-body px-0 py-1">
                        <div class="text-center">
                            <!-- Product name-->
                            <p class="fw-bolder my-0" id="pn_${data.product_id}">${data.name}</p>
                            <!-- Product price-->
                            <p class="my-0">${parseInt(data.capacity).toLocaleString('id-ID')}</p>
                        </div>
                    </div>
                    <!-- Product actions-->
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent mt-2">
                        <div class="text-center">
                            <a class="btn btn-outline-dark btn-light-gray mt-auto setOrder" href="javascript:void(0)" id="mp_${data.product_id}">Buat Order</a>
                        </div>
                    </div>
                </div>
            `);
            // skrip ended
        })
       }); 
    });

    $('#apm').on('click','.setOrder',function(){
        let button_id = this.id.split('_');
        let pn_id = $('#pn_'+button_id[1]).text();
        localStorage.setItem('product_id',button_id[1]);
        localStorage.setItem('product_name',pn_id);
        window.location.href="{{ url('/order') }}";
    })
</script>
@endsection
