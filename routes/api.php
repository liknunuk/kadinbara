<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ExchangeController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\SkillsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductMapController;
use App\Http\Controllers\TalentController;
use App\Models\Exchange;
use App\Models\ProductMap;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/misal', function () {
    $data = [
        ['nama'=>'Bagus' , 'fakultas'=>'Kedokteran Ternak'],
        ['nama'=>'Juwita' , 'fakultas'=>'Ekonomi lemah'],
    ];
    return response()->json([
        'success' => true,
        'message' => $data
    ]);
});
// Pemilik Perusahaan
Route::get('/owner/{name}',[MemberController::class,'companyOwner']);

// Produk dan kategori
Route::get('/kateprod', [ProductController::class, 'allProducts']);
Route::get('/procats', [ProductCategoryController::class, 'procats']);

// data produk
Route::get('/prodinfo/{id}', [ProductController::class, 'prodinfo']);

// data perusaan
Route::get('/company/{name}', [CompanyController::class, 'companies']);

// Peta Komoditas
Route::get('/promap', [ProductMapController::class, 'promap']);
Route::get('/pmc/{id}', [ProductMapController::class, 'pmPerCamat']);
Route::get('/pdc/{kecamatan}', [ProductMapController::class, 'pmOneCamat']);

// Daftar produk
Route::get('productSearch/{name}', [ProductController::class, 'productnames']);
Route::get('allProductMap', [ProductMapController::class, 'allProductMap']);

// Bursa Komoditas
Route::get('latestExc', [ExchangeController::class, 'latestExchanges']);

// Pencari kerja
Route::get('talentas', [SkillsController::class, 'index']);
