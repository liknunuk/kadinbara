<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductMap extends Model
{
    use HasFactory;
    protected $table = "product_maps";
    protected $guarded = ['id'];

    // RElationship
    // 1 productmap milik 1 productMap
    public function Product(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
