<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    // relationship
    // on ProductCategory has many Products
    public function Category(){
        return $this->hasMany(Product::class);
    }
}
