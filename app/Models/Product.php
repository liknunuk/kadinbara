<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    // relationsip
    // 1 product belongs to 1 category
    public function ProductCategory(){
        return $this->belongsTo(ProductCategory::class);
    }

    // 1 product punya banyak productMap
    public function ProuductMap(){
        return $this->hasMany(ProductMap::class, 'id', 'product_id');
    }
}
