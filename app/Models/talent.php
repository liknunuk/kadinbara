<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class talent extends Model
{
    use HasFactory;
    protected $table = 'talent';
    protected $guarded = ['id'];
}
