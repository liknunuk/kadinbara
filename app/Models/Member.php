<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;
    protected $table = 'members';
    protected $guarded = ['id'];

    /* -- Relationship -- */
    // One member has many companies
    public function companies(){
        return $this->hasMany(Company::class);
    }
}
