<?php

namespace App\Http\Controllers;

use App\Http\Resources\MemberRsrc;
use Illuminate\Http\Request;
use App\Models\Member;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the members
    //    $members = Member::all();
       $members = Member::latest()->paginate(10);

       // load the view and pass the members
       return View('kadin.anggota',[
        'type_menu'=>'kadin',
        'title'=>'Daftar Anggota'
        ]
        )->with('members', $members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $member = null;
        return View('kadin.anggota-form',['type_menu'=>'kadin','title'=>'Anggota Baru'])->with('member',$member);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Member::create($request->all());
       
        return redirect()->route('anggota-person.index')
                        ->with('Sukses','Anggota berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // echo $id;
        $member = Member::where('id',$id)->first();
        // echo "Nomor: " . $member->id;
        return view('kadin.anggota-info',['type_menu'=>'kadin','title'=>'Info Anggota'])->with('member',$member);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id)->first();
        return View('kadin.anggota-form',['type_menu'=>'kadin','title'=>'Anggota Baru'])->with('member',$member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        $member->fullName   = $request->fullName;
        $member->address    = $request->address;
        $member->email      = $request->email;
        $member->phone      = $request->phone;
        $member->medsocs    = $request->medsocs;
        $member->save();
        return redirect()->route('anggota-person.index')
                        ->with('Sukses','Anggota berhasil dimutakhirkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);
        $member->delete();       
       
        return redirect()->route('anggota-person.index')
                        ->with('success','Data anggota berhasil dihapus');
    }

    // Find Member In API
    public function companyOwner($name){
        // echo "successed";
        $owner = Member::where('fullName', 'like', "%{$name}%")->get();
        return response()->json([
            'data'=>$owner
        ]);
    }

}
