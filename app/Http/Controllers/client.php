<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Exchange;

class client extends Controller
{
    public function index(){
        return view('clients.index');
    }

    public function create(){
        return view('clients.inquiry-form',['type_menu'=>'bursa','title'=>'Sebaran Produk']);
    }
}
