<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\Company;
use App\Models\Exchange;

class ExchangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the members
       $exchanges = Exchange::all();

       // load the view and pass the exchanges
       return View('kadin.products-exchange',['type_menu'=>'bursa'])
           ->with('exchanges', $exchanges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //id , inquiryDate , applicantName , applicantAddresss , applicantEmail , applicantPhone , applicantCity , applicantProvince , product_id , orderQuota, dueDate
        $exc = new Exchange;
        $exc->id = $request->id;
        $exc->inquiryDate = $request->inquiryDate;
        $exc->applicantName = $request->applicantName;
        $exc->applicantAddress = $request->applicantAddress;
        $exc->applicantEmail = $request->applicantEmail;
        $exc->applicantPhone = $request->applicantPhone;
        $exc->applicantCity = $request->applicantCity;
        $exc->applicantProvince = $request->applicantProvince;
        $exc->product_id = $request->product_id;
        $exc->orderQuota = $request->orderQuota;
        $exc->dueDate = $request->dueDate;
        $exc->save();
        if( $request->route == 'client'):
            return redirect('client')->with('success','Pesanan telah disimpan');
        else:
            return redirect()->route('bursa-komoditas.index')->with('success','Pesanan telah disimpan');
        endif;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // } 

    /* API feeds */
    public function latestExchanges(){
        // SELECT product_id , inquiryDate , applicantName , applicantCity , orderQuota , dueDate FROM exchanges ORDER BY product_id , inquiryDate Desc
        $data = Exchange::join('products', 'product_id', '=', 'products.id')
            ->selectRaw("exchanges.id , product_id , products.name , inquiryDate , applicantName , applicantCity , orderQuota , dueDate ")
            ->orderBy('product_id')
            ->orderBy('inquiryDate', 'Desc')
            ->get();
        return view('kadinapi.latestExc')->with('data',$data);
    }
}
