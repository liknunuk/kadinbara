<?php

namespace App\Http\Controllers;

use App\Models\talent;
use App\Models\skills;
use Illuminate\Http\Request;

class TalentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $naker = talent::latest()->paginate(20);
        return view('kadin.talent',[
            'type_menu'=>'Talent',
            'title'=>'Pencari Kerja'
        ])->with(['talents'=>$naker]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        return view('kadin.talent-form')->with(['request'=>$request]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->insertSkills($request->keahlian);
        talent::create($request->all());
        if($request->redir == "talent/register"){
            return redirect()->to('/talent/register')->with(['success'=>'Pendaftaran berhasil']);
        }else{
            return redirect()->route('talents.index')->with(['success'=> 'Data talent tersimpan']);
        }
    }

    private function insertSkills($skills){
        $keahlian = explode(", ", $skills);
        foreach($keahlian as $skill){
            skills::insertOrIgnore(['skill'=>$skill]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\talent  $talent
     * @return \Illuminate\Http\Response
     */
    public function show(talent $talent)
    {
        return view('kadin.talent-info')->with(['type_menu'=>'Talent','title'=>'Pencari Kerja','talent' => $talent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\talent  $talent
     * @return \Illuminate\Http\Response
     */
    public function edit(talent $talent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\talent  $talent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, talent $talent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\talent  $talent
     * @return \Illuminate\Http\Response
     */
    public function destroy(talent $talent)
    {
        //
    }

    // API responses
    public function skills(){
        $skills = talent::get('keahlian');
        return view('talent.skills', ['skills' => $skills]);
    }
}
