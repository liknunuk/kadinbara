<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the members
       $companies = Company::all();

       // load the view and pass the companies
       return View('kadin.badan-usaha',
        [
            'type_menu'=>'kadin',
            'title'=>'Badan Usaha'
        ]
        )->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = null;
        return View('kadin.badan-usaha-form',['type_menu'=>'kadin','title'=>'Perusahaan'])->with('company',$company);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $buscat = "";
        // foreach( $request->bc as $bc=>$cat){
        //     $buscat .= $bc . ',';
        // }
        Company::create($request->all());
       
        return redirect()->route('anggota-badan-usaha.index')
                        ->with('Sukses','Perusahaan berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return view('kadin.badan-usaha-info',['type_menu'=>'kadin','title'=>'Info Perusahaan'])->with('company',$company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('kadin.badan-usaha-form',['type_menu'=>'kadin','title'=>'Info Perusahaan'])->with('company',$company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // name,address,bussinesCategory,phone,email,featuredProduct,member_id
        $company = Company::find($id);
        $company->name = $request->name;
        $company->address = $request->address;
        $company->businessCategory = $request->businessCategory;
        $company->phone = $request->phone;
        $company->email = $request->email;
        $company->featuredProduct = $request->featuredProduct;
        $company->member_id = $request->member_id;
        $company->save();
        return redirect()->route('anggota-badan-usaha.index')->with('success', 'Perusahaan telah dimutakhirkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company=Company::find($id);
        $company->delete();
        return redirect()->route('anggota-badan-usaha.index')->with('Sukses', 'Perusahaan telah dihapus');

    }

    // API feed
    public function companies($name){
        $company=Company::where('name', 'like', "%{$name}%")->get(['id', 'name']);
        return response()->json([
            'data'=>$company
        ]);
    }    
}
