<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductMap;

// Columns: id,kecamatan,productCapacity,product_id,company_id
class ProductMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the members
       $productsMap = ProductMap::all();

       // load the view and pass the productsMap
       return View('kadin.product-map',['type_menu'=>'bursa','title'=>'Sebaran Produk'])
           ->with('productsMap', $productsMap);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $map = null;
        return View('kadin.product-map-form',['type_menu'=>'bursa','title'=>'Peta Komoditas'])->with('map',$map);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ProductMap::create($request->all());
        return redirect()->route('produk.index')->with('success', 'Data peta komoditas telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $map = ProductMap::find($id);
        return View('kadin.product-map-form',['type_menu'=>'bursa','title'=>'Peta Komoditas'])->with('map',$map);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Columns: id,kecamatan,productCapacity,product_id,company_id
        $map = ProductMap::find($id);
        $map->kecamatan = $request->kecamatan;
        $map->productCapacity = $request->productCapacity;
        $map->product_id = $request->product_id;
        $map->company_id = $request->company_id;
        $map->save();
        return redirect()->route('produk.index')->with('success', 'Data peta komoditas telah dimutakhirkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $map = ProductMap::find($id);
        $map->delete();
        return redirect()->route('peta-komoditas.index')->with('success', 'Data peta komoditas telah ditambahkan');
    }

    // API section
    // statistik peta komoditas per kecamatan
    // SELECT kecamatan, product_id , SUM(productCapacity) capacity FROM product_maps GROUP BY product_id
    public function pmPerCamat($productId=0)
    {
                   
        $pmc = ProductMap::where('product_id',$productId)
            ->selectRaw('kecamatan, product_id , sum(productCapacity) as productCapacity')
            ->groupBy('kecamatan')
            ->get();
            return response()->json([
                'data'=>$pmc
            ]);
        // echo $productId;
    }

    // Potensi Produk sebuah kecamatan
    // elect product_id,sum(productCapacity) kapacity from product_maps where kecamatan='Karangkobar' group by product_id
    public function pmOneCamat($kec)
    {
        $poc = ProductMap::where('kecamatan', $kec)
            ->selectRaw('product_id,sum(productCapacity) as productCapacity')
            ->groupBy('product_id')
            ->get();
        return view('kadinapi.potcam')->with('products', $poc);
    }

    public function promap(){
        // SELECT kecamatan, product_id , SUM(productCapacity) capacity FROM product_maps GROUP BY kecamatan , product_id ORDER BY kecamatan;
        $promap = ProductMap::selectRaw('kecamatan, product_id , SUM(productCapacity) as capacity')
            ->groupBy('product_id','kecamatan')
            ->orderBy('kecamatan')
            ->get();
        return view('kadinapi.promap')->with('promap', $promap);
    }
    // All Product Capacity
    Public function allProductMap(){
        // SELECT a.product_id , b.name , SUM(a.productCapacity) capacity FROM product_maps a , products b WHERE a.product_id = b.id GROUP BY a.product_id;
        $data = ProductMap::join('products', 'product_id', '=', 'products.id')
            ->selectRaw('product_id,name,SUM(productCapacity) as capacity,imgUrl')
            ->groupBy('product_id')->get();
        return response()->json([
            'data' => $data
        ]);
    }
}
