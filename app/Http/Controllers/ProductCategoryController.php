<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the members
       $productCategories = ProductCategory::orderBy('name')->get();

       // load the view and pass the productCategories
       return View('kadin.product-categories',['type_menu'=>'bisnis','title'=>'Kategori Produk'])
           ->with('productCategories', $productCategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create(Request $request)
    // {
    //     // $procat = null;
    //     // return view('product-categories-')
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ProductCategory::create($request->all());
       
        return redirect()->route('kategori-produk.index')
                        ->with('success','Kategori berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     // echo $id;
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = ProductCategory::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect()->route('kategori-produk.index')
                        ->with('success','Kategori berhasil dimutakhirkan');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $procat = ProductCategory::find($id);
        $procat->delete($id);
        return redirect()->route('kategori-produk.index')->with('success', 'Kategori sudah dihapus');
    }

    // API RESPONSES
    public function procats(){
        $procat = ProductCategory::all();
        return response()->json(['kategori'=>$procat]);
    }
}
