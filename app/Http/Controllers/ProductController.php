<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the members
       $products = Product::all();

       // load the view and pass the products
       return View('kadin.products',['type_menu'=>'bisnis','title'=>'Produk'])
           ->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Product::create($request->all());
        return Redirect()->Route('kategori-produk.index')->with('success','Produk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Product::find($id);
        return view('kadin.products-form',['type_menu'=>'bisnis','title'=>'Produk'])->with('produk', $produk);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Product::find($request->id);
        $produk->name = $request->name;
        $produk->save();
        return redirect()->route('produk.index')->with('success', 'Data produk telah diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk=Product::find($id);
        $produk->delete();
        return redirect()->route('produk.index')->with('success', 'Data produk telah dihapus');
    }

    // Menampilkan seluruh produk dan category untuk api
    public function allProducts(){
        // $products = Product::all();
        $products = Product::orderBy('product_category_id')->get();
        return View('kadinapi.products')->with('products', $products);
    }

    public function prodinfo( $id ){
        // echo "$id";
        $products = Product::find($id);
        $result = ['kategori' => $products->ProductCategory->name, 'produk' => $products->name];
        echo json_encode($result);
    }

    public function productnames($name){
        $products = Product::where('name', 'like', "%{$name}%")->get(['id', 'name']);
        // return view('kadinapi.potcam')->with('products', $products);
        return response()->json(['data' => $products]);
    }

    public function updateImage(Request $request){
        
        $produk = Product::find($request->id);
        $upload = $this->uploadImage($request);
        $produk->imgUrl = $upload;
        $produk->save();
        return redirect()->route('produk.index')->with('success', 'Foto produk telah diunggah');

    }
    private function uploadImage(Request $request){
        
        $request->validate([
            'imgUrl' => 'required|image|mimes:png|max:2048'
        ]);

        return $request->file('imgUrl')->store('productImages','public');
    }
}
