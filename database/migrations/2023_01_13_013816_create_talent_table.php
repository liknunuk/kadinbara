<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent', function (Blueprint $table) {
            $table->id();
            $table->string('nik',20);
            $table->string('namaLengkap', 50);
            $table->enum('jnKelamin',['Laki-laki', 'Perempuan'])->default('Laki-laki');
            $table->string('tempatLahir',50)->default('Banjarnegara');
            $table->date('tanggalLahir');
            $table->string('alamat');
            $table->enum('pendidikanTerakhir', ['SLTP', 'SLTA', 'Diploma 3', 'Sarjana'])->default('SLTA');
            $table->string('keahlian');
            $table->string('email');
            $table->string('telepon');
            $table->longText('deskripsi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talent');
    }
};
