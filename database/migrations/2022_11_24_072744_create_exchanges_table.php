<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->id();
            $table->date('inquiryDate');
            $table->string('applicantName',50);
            $table->string('applicantEmail',100);
            $table->string('applicantAddress',100);
            $table->string('applicantPhone',20);
            $table->string('applicantCity',50);
            $table->string('applicantProvince',50);
            $table->foreignId('product_id');
            $table->unsignedInteger('orderQuota');
            $table->date('dueDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchanges');
    }
};
